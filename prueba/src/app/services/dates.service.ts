import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';



import {Dates} from './../interfaces/dates';

@Injectable({
  providedIn: 'root'
})
export class DatesService {

  constructor(
    private http: HttpClient,
  ) { }


  getRandomComic() {
    const path = '/info.0.json';
    return this.http.get<Dates>(path);
  }

}
