import { Component, OnInit } from '@angular/core';
import { DatesService } from './services/dates.service';
import { Dates} from './interfaces/dates';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'prueba';
  comics: Dates[] = [];
  title2 = 'star-angular';
  stars = [1, 2, 3, 4, 5];
  rating = 0;
  hoverState = 0;
  titleA = 'angulartoastr';



  // transform(value: any, args: any[] = null): any {
  //   return Object.keys(value);
  // }
  constructor(

    private datesService: DatesService,
    private toastr: ToastrService
    ) {
    }
    showSuccess() {
      this.toastr.success('por la calificación', 'Gracias',
      {timeOut: 2000});
    }

    ngOnInit() {
      // let evilResponse = datesService;

      // this.comic = this.datesService.getRandomComic();
      // const mykey = Object.keys(this.comics);
      // console.log('mykey', mykey);


      this.datesService.getRandomComic()
        .subscribe(
          comic => {
          // console.log(comic);
          console.log(comic);

          // const comics = (Object.keys (comic));
          this.comics.push(comic);

          console.log(this.comics);

      });

    }
    getRandomComic() {
      this.datesService.getRandomComic()
      .subscribe(comic => {
          console.log(comic);
          this.comics.push(comic);
      });
    }

    // qualifyComic() {
    //   const comic = {
    //     month: 'string',
    //     year: 'string',
    //     safeTittle: 'string',
    //     alt: 'string',
    //     img: 'string',
    //     tittle: 'string',
    //     respuesta: 'string'
    //   };
    //   this.datesService.qualifyComic(comic)
    //   .subscribe((resp) => {
    //     console.log(resp);
    //   });
    // }
    enter(i) {
      this.hoverState = i;
    }

    leave(i) {
      this.hoverState = 0;
    }

    updateRating(i) {
      this.rating = i;
    }

}

